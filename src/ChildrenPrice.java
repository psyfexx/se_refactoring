public class ChildrenPrice implements Price {

    @Override
    public int getPriceCode() {
        return 2;
    }

    @Override
    public int getFrequentRenterPoints(int days) {
        return 1;
    }

    @Override
    public double getCharge(int days) {
        double result= 1.5;
        if (days > 3)
            result+= (days-3)*1.5;
        return result;
    }
}
