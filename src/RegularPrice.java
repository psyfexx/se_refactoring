public class RegularPrice implements Price {

    @Override
    public double getCharge(int days){
       double result=2.0;
        if (days >2 )
            result+=(days-2)*1.5;
        return result;
    }

    @Override
    public int getPriceCode() {
        return 0;
    }

    @Override
    public int getFrequentRenterPoints(int days) {
        return 1;
    }

}
