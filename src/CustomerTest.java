import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTest {
    Customer customer;

    @BeforeEach
    void setUp() {
        customer = new Customer("Anna");
    }

    @Test
    void addRental() {
        Movie transporter = new Movie("transporter", 1);
        Rental rental = new Rental(transporter, 5);
        customer.addRental(rental);
        assertEquals(customer.getRentals().get(0), rental);
    }

    @Test
    void getName() {
        String name = customer.getName();
        assertEquals(name, "Anna");
    }

    @Test
    void statement() {
        Movie transporter = new Movie("transporter", 1);
        Rental rental1 = new Rental(transporter, 5);
        Movie ghostbuster = new Movie("Ghost Busters", 0);
        Rental rental2 = new Rental(ghostbuster, 2);
        customer.addRental(rental1);
        customer.addRental(rental2);

        String expected = "Rental Record for " + customer.getName() + "\n";
        expected+= "\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n";
        expected+= "\t" + transporter.getTitle() + "\t" + "\t" + rental1.getDaysRented() + "\t" + rental1.getCharge() + "\n";
        expected+= "\t" + ghostbuster.getTitle() + "\t" + "\t" + rental2.getDaysRented() + "\t" + rental2.getCharge() + "\n";
        expected+= "Amount owed is " + 17.0 + "\n";
        expected+= "You earned " + 3 + " frequent renter points";
        assertEquals(customer.statement(), expected);
    }

}