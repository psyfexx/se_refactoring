public class ReleasePrice implements Price {
    @Override
    public double getCharge(int days) {
        return days*3;
    }

    @Override
    public int getPriceCode() {
        return 1;
    }

    @Override
    public int getFrequentRenterPoints(int days) {
        return (days >1) ? 2:1;
    }


}
