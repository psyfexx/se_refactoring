public interface Price {
    double getCharge(int days);
    int getPriceCode();
    int getFrequentRenterPoints(int days);
}
