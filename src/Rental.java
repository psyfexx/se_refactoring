class Rental {
    private final Movie _movie;
    private final int _daysRented;

    public Rental(Movie newmovie, int newdaysRented) {
        _movie = newmovie;
        _daysRented = newdaysRented;
    }

    public int getDaysRented() {
        return _daysRented;
    }

    public Movie getMovie() {
        return _movie;
    }

    public int getFrequentRenterPoints() {
        return _movie.getFrequentRenterPoints(_daysRented);
            }

    public double getCharge(){
        return _movie.getCharge(_daysRented);
    }
}